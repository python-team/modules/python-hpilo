python-hpilo (4.4.3-4) unstable; urgency=medium

  * d/control: Adopt package. Add mysel as Uploaders. Closes: #888079.
  * Packaging update
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove field Testsuite on binary package python-hpilo-doc that
    duplicates source.
  * d/upstream/metadata: Add metadata file.

 -- Emmanuel Arias <eamanu@debian.org>  Wed, 13 Mar 2024 19:15:08 -0300

python-hpilo (4.4.3-3) unstable; urgency=medium

  * orphan

 -- Sandro Tosi <morph@debian.org>  Wed, 28 Feb 2024 01:13:16 -0500

python-hpilo (4.4.3-2) unstable; urgency=medium

  * debian/rules
    - define PYBUILD_NAME

 -- Sandro Tosi <morph@debian.org>  Sat, 18 Jun 2022 20:02:44 -0400

python-hpilo (4.4.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * New upstream release
  * Use the new Debian Python Team contact name and address
  * debian/watch
    - track github releases
  * debian/copyright
    - update upstream copyright years
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 4.6.1 (no changes needed)
    - run wrap-and-sort
    - switch distutils for setuptools in b-d
    - bump debhelper-compat to 13
  * debian/rules
    - disable tests, for now

 -- Sandro Tosi <morph@debian.org>  Mon, 06 Jun 2022 21:56:02 -0400

python-hpilo (4.3-3) unstable; urgency=medium

  * debian/control
    - add Breaks+Replaces: python-hpilo; Closes: #938956

 -- Sandro Tosi <morph@debian.org>  Fri, 06 Sep 2019 00:32:57 -0400

python-hpilo (4.3-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Sandro Tosi ]
  * Drop python 2 support + use pybuild
  * debian/control
    - switch to py3k sphinx

 -- Sandro Tosi <morph@debian.org>  Mon, 26 Aug 2019 00:02:31 -0400

python-hpilo (4.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Sandro Tosi ]
  * New upstream release
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 4.3.0 (no changes needed)
  * bump compat level to 11

 -- Sandro Tosi <morph@debian.org>  Sun, 03 Feb 2019 14:34:46 -0500

python-hpilo (4.2.1-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - update upstream copyright years
  * debian/control
    - use sphinx-rtd-theme in b-d

 -- Sandro Tosi <morph@debian.org>  Mon, 11 Jun 2018 00:26:21 -0400

python-hpilo (3.9-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ Sandro Tosi ]
  * debian/control
    - add python3-distutils to b-d; Closes: #896776
    - bump Standards-Version to 4.1.4 (no changes needed)
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Thu, 26 Apr 2018 23:28:15 -0400

python-hpilo (3.9-1) unstable; urgency=medium

  * New upstream release
  * compat level 10

 -- Sandro Tosi <morph@debian.org>  Fri, 16 Dec 2016 22:19:45 -0500

python-hpilo (3.8-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * New upstream release
  * debian/control
    - bump Standards-Version to 3.9.8 (no changes needed)

  [ Ondřej Nový ]
  * Fixed VCS URL

 -- Sandro Tosi <morph@debian.org>  Sat, 18 Jun 2016 12:23:02 +0100

python-hpilo (3.6-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright
    - update upstream copyright years
    - extend packaging copyright years
  * debian/docs
    - use README.md, new file name
  * debian/python-hpilo-doc.examples
    - install examples in the -doc pkg
  * debian/control
    - point Vcs-Git to HTTPS

 -- Sandro Tosi <morph@debian.org>  Mon, 08 Feb 2016 21:51:27 +0000

python-hpilo (2.12.1-1) unstable; urgency=medium

  * New upstream release
  * debian/watch
    - use PyPI redirector
  * debian/copyright
    - extend upstream copyright years

 -- Sandro Tosi <morph@debian.org>  Sat, 11 Apr 2015 14:49:16 +0100

python-hpilo (2.11-1) unstable; urgency=low

  * Initial release (Closes: #775919)

 -- Sandro Tosi <morph@debian.org>  Sun, 25 Jan 2015 20:34:39 +0000
